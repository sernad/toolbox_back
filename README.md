# Aplicación middleware

Este Middleware utiliza Node.js versión 16.14.0 LTS (https://nodejs.org/) y Express 4 (https://expressjs.com/), se recomienda particularmente validar la versión de Node.js que se utiliza para evitar conflictos, las versiones de Express y el resto de los paquetes npm utilizados residen en `/package.json`.

Cabe destacar que abstrayendose a lo solicitado,la información correspondiente a los archivos sera guardada en un archivo de texto.
## Configuración general

En `/config/default.json5` está la configuración general de la aplicación, lo que está ahí aplica a todos los ambientes y todas las instancias.

Existen adicionalmente los archivos de configuración `/config/production.json5` y `/config/testing.json5`, cuando se utiliza alguno de ellos, las configuraciones que contengan tendrán preponderancia sobre las de `/config/default.json5`, y lo que no se defina en ellos se seguirá tomando de `/config/default.json5`.


## Estandarización del formato de código

Este proyecto se rige bajo los principios de JavaScript Semi-Standard Style - https://www.npmjs.com/package/semistandard/

Configuración en VS Code:

1. Instalar la extensión "JavaScript Standard Style"
  - https://marketplace.visualstudio.com/items?itemName=standard.vscode-standard
2. Configurar `settings.json` con los siguientes parámetros:
  ```json
  "javascript.validate.enable": false,
  "standard.engine": "semistandard",
  "editor.tabSize": 2,
  ```
3. Configurar `package.json` con los siguientes parámetros:
  ```json
  "scripts": { "test": "semistandard" }
  ```
  ```json
  "devDependencies": { "semistandard": "*" }
  ```

## Resumen de la aplicación

El presente proyecto corresponde a la prueba de ingreso de toolbox. Consta de 1 API encargada de:

**Solicitar lista de archivos**
  - Determina si se buscará la una lista de archivos o un archivo en particular
  - Buscar la información correspondiente (del / los) arcvhivo (s) correspondiente(s)
  - Formatear la respuesta recibida 
  - Retornar la respuesta

**Formato de respuesta**
  
  Por cada archivo recibido se retornara un arreglo u objeto  con las siguientes propiedades:
  ```
  {
    filename,
    lines: [
      text,
      number,
      hex
    ]
  }
  ```

**Peticiones**
  - **GET /files/list** Retorna un arreglo con las propiedades correspondientes a la lista de archivos suministradas por la api externa

  - **GET /files/data?fileName=<filename>** Retorna las propiedades correspondientes al archivo suministrado

**Comandos**
  - npm start --> levantar proyecto en el ambiente local
  - npm run test --> Ejecuta las prubas unitarias
