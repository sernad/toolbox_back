// let Book = require('../app/models/book');

// Require the dev-dependencies
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');
const should = chai.should();
const { formatFileContent } = require('../representations/files/getFiles');

chai.use(chaiHttp);

// Our parent block
describe('Files', () => {
  /*
  * Test the /GET /files/list
  */
  describe('/GET /files/list', () => {
    it('it should GET all the file list', (done) => {
      chai.request(server)
        .get('/files/list')
        .end((err, res) => {
          if (err) done();
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
  });

  /*
  * Test the /GET /files/data
  */
  describe('/GET /files/data', () => {
    it('it should GET specific file data', (done) => {
      const filename = 'test2.csv';
      chai.request(server)
        .get(`/files/data?fileName=${filename}`)
        .end((err, res) => {
          if (err) done();
          res.should.have.status(200);
          res.body.should.be.a('object');
          res.body.should.have.own.property('file');
          res.body.lines.should.be.a('array');
          done();
        });
    });
  });

  /*
  * Test the /GET /files/data
  */
  describe('/GET /files/data', () => {
    it('it should format object', (done) => {
      const dummyData = 'file,text,number,hex\ntest2.csv,oMEHn\ntest2.csv,ucB,9,dc7e9b103536ed190f11910b8f3d0048';
      const response = formatFileContent(dummyData);

      response.should.be.a('object');
      response.should.have.own.property('file');
      response.should.have.own.property('lines');
      response.lines.should.be.a('array');
      done();
    });
  });
});
