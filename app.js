const cors = require('cors');
const conf = require('config');
const express = require('express');
const app = express();
// const bodyParser = require('body-parser');
const server = require('http').Server(app);
const routing = require('./representations/routing')(app);
const port = process.env.PORT ? parseInt(process.env.PORT) : parseInt(conf.get('SERVER.PORT'));

app.set('port', port);

app.use(cors());
app.disable('x-powered-by');
app.use(express.json({ limit: '3MB' }));
app.use(express.urlencoded({ extended: false }));

// Setting up endpoints routing
routing.setUpRoutes();

// Listen on provided port, on all network interfaces
server.listen(port, () => {
  console.log(`Running on port ${port}`);
});

module.exports = app;
