'use strict';

const express = require('express');
const router = express.Router();

const { getFiles } = require('./getFiles');

router.get('/list', getFiles);
router.get('/data', getFiles);

module.exports = router;
