const axios = require('axios');

/**
 * Fetch the content of a specifc file from the provided external API
 * @param {string} fileName Name of the file
 * @returns {object} Object with the content of the file (ideally a valid .csv file)
 */
const fetchFileContent = async (fileName) => {
  return new Promise((resolve, reject) => {
    const url = `https://echo-serv.tbxnet.com/v1/secret/file/${fileName}`;
    const headers = { Authorization: 'Bearer aSuperSecretKey' };

    axios.get(url, { headers })
      .then((fileData) => { resolve(fileData); })
      .catch(error => { reject(error); });
  });
};

/**
 * Checks the content of a string and returns the formated content
 * @param {string} data String with the content of a .csv file
 * @returns {object} Object containing the file name (if applies) and the valid lines inside the .csv
 */
const formatFileContent = (fileTextOriginal) => {
  let filename = '';
  const fileText = fileTextOriginal.replace(/file,text,number,hex(\\n){0,1}/g, '');
  const fileLines = fileText.split('\n');
  const validLines = [];

  // Validate that the file has lines to validate
  if (fileLines.length === 1 && fileLines[0] === '') return null;

  fileLines.forEach(d => {
    let errors = 0;
    const tmp = d.split(',');

    if (filename === '' && (/.csv$/.test(tmp[0]))) filename = tmp[0];
    if (!tmp[0] && !(/.csv$/.test(tmp[0]))) errors++;
    if (!tmp[1] && !(/^[A-Za-z0-9]*$/.test(tmp[1]))) errors++;
    if (!tmp[2] && !(/^[0-9]*$/.test(tmp[2]))) errors++;
    if (!tmp[3] && !(/^[A-Za-z0-9]{32}$/.test(tmp[3]))) errors++;

    if (errors === 0) validLines.push({ text: tmp[1], number: tmp[2], hex: tmp[3] });
  });

  filename = filename === '' ? 'INVALID_NAME' : filename;
  return { file: filename, lines: validLines };
};

/**
 * Fetches the file list from the external API
 * @returns {array} Array with the name of the available files
 */
const fetchFileList = async () => {
  const url = 'https://echo-serv.tbxnet.com/v1/secret/files';
  const headers = {
    Accept: 'application/json',
    Authorization: 'Bearer aSuperSecretKey'
  };

  return await axios.get(url, { headers });
};

const getFiles = async (req, res) => {
  try {
    const response = [];
    const filesData = [];
    const params = req.query.fileName;

    // Set list to get data from
    const fileList = params ? [params] : (await fetchFileList()).data.files;

    // Fetch files content
    fileList.forEach(fileName => { filesData.push(fetchFileContent(fileName)); });

    // After all the information is fetched, validate and format the response
    Promise.allSettled(filesData).then(query => {
      query.forEach(q => {
        if (q.status === 'fulfilled' && q.value) {
          const validatedContent = formatFileContent(q.value.data);
          if (validatedContent) response.push(validatedContent);
        }
      });

      // Send formated response
      res.send(params ? response[0] : response);
    });
  } catch (error) {
    res.status(500).send({ error: 'Error data' });
  }
};

module.exports = { getFiles, fetchFileContent, formatFileContent, fetchFileList };
