module.exports = (app) => {
  const filesRouter = require('./files/_routes');

  return {
    setUpRoutes: () => {
      app.use('/files', filesRouter);
    }
  };
};
